﻿#include <iostream>
using namespace std;

void findOddByLimit(int limit, bool isOdd) {
	for (int i = 0; i < limit; i++)
	{
		if (isOdd ? i % 2 == 0 : i % 2 != 0) cout << i <<  endl;
	}
}

int main()
{
	const int N = 8;
	findOddByLimit(N,false);
}